# Object Detection Using YOLO

This repo was used for the object detection workshop in Auckland on the 25th of August 2022. 

## Notebook contents

The contents inlcude:
- Notebook 1: A notebook that downloads yolov5 repo and runs through how to use the open source model trained on the coco dataset to make predictions on images, videos and using your devices webcam.
- Notebook 2: Contains a link to labelImg repo and some basic code on how to launch it with your own sample data. I've included a data folder that you could use to store your own custom images and labelled data.
- Notebook 3: Code on how to train your own custom model, load that model into the notebook, test on images and videos, as well as export to various formats.
- Notebook 4: A rough demonstration of how the bounding box data can be potentially used to then create tracking software for objects within a time series plane. 
- data: A folder containing the frames for notebook 4. 

## YOLOv5 Github

https://github.com/ultralytics/yolov5

The YOLOv5 github contains all the instructions to do basic inference as well as how to set up your own custom training data and model. In particular, there are instructions on how to create the dataset.yml file for fine tuning on your own dataset. 

For more information, feel free to contact me at nathan@arcanum.ai 
